package mx.roomie.resource;

import mx.roomie.Application;
import mx.roomie.model.Rule;
import mx.roomie.request.RuleRequest;
import mx.roomie.service.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class RuleResource {
    @Autowired
    private RuleService ruleService;
    private final static String RULES_PATH = "/rules";
    private final static String RULE_PATH = "/rule";


    @PostMapping(RULE_PATH)
    public ResponseEntity<Rule> saveRule(@RequestBody RuleRequest request) {
        Application.logger.debug("Creating rule");
        Rule rule = ruleService.saveRule(request);

        return ResponseEntity.status(HttpStatus.CREATED).body(rule);
    }

    @GetMapping(RULES_PATH)
    public ResponseEntity<List<Rule>> getRules() {
        Application.logger.debug("Getting all rules");
        return ResponseEntity.ok().body(
                ruleService.getRules()
        );
    }

    @GetMapping(RULE_PATH+"/{id}")
    public ResponseEntity<Rule> getRule(@PathVariable Integer id) {
        Application.logger.debug("Getting rule with id: " + id);
        Optional<Rule> ruleOptional = ruleService.getRuleById(id);

        return ruleOptional.isPresent() ?
                ResponseEntity.ok(ruleOptional.get()) :
                ResponseEntity.notFound().build();
    }

    @DeleteMapping(RULE_PATH+"/{id}")
    public ResponseEntity<Rule> deleteRule(@PathVariable Integer id) {
        Application.logger.debug("Deleting rule with id: " + id);
        ruleService.deleteRule(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping(RULE_PATH+"/{id}")
    public ResponseEntity<Rule> updateRule(
            @PathVariable Integer id,
            @RequestBody RuleRequest request
    ) {
        Application.logger.debug("Updating rule with id: " + id);
        Optional<Rule> ruleOptional = ruleService.getRuleById(id);

        if (!ruleOptional.isPresent()) return  ResponseEntity.notFound().build();

        Rule rule = ruleService.saveRule(request, id, ruleOptional);

        return ResponseEntity.ok().body(rule);
    }
}
