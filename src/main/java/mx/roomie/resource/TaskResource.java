package mx.roomie.resource;

import java.util.List;
import java.util.Optional;

import mx.roomie.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import mx.roomie.model.Task;
import mx.roomie.request.TaskRequest;
import mx.roomie.service.TaskService;

@RestController
public class TaskResource {

    @Autowired
    private TaskService taskService;

    @PostMapping("/task")
    public ResponseEntity<Task> saveTask(@RequestBody TaskRequest request) {
        Application.logger.debug("Creating task");
        Task task = taskService.saveTask(request);

        return ResponseEntity.status(HttpStatus.CREATED).body(task);
    }

    @GetMapping("/tasks")
    public ResponseEntity<List<Task>> getTasks() {
        Application.logger.debug("Getting all tasks");

        List<Task> tasks = taskService.getTasks();
        ResponseEntity<List<Task>> response = ResponseEntity.ok().body(tasks);
        return response;
    }

    @GetMapping("/task/{id}")
    public ResponseEntity<Task> getTask(@PathVariable Integer id) {
        Application.logger.debug("Getting task with id: " + id);

        Optional<Task> taskOptional = taskService.getTaskById(id);

        if (!taskOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Task task = taskOptional.get();
        return ResponseEntity.ok().body(task);

    }

    @DeleteMapping("/task/{id}")
    public ResponseEntity<Task> deleteTask(@PathVariable Integer id) {
        Application.logger.debug("Deleting task with id: " + id);
        taskService.deleteTask(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping("/task/{id}")
    public ResponseEntity<Task> updateTask(@PathVariable Integer id, @RequestBody TaskRequest request) {
        Application.logger.debug("Updating task with id: " + id);
        Optional<Task> taskOptional = taskService.getTaskById(id);

        if (!taskOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Task task = taskService.saveTask(request, id, taskOptional);

        return ResponseEntity.ok().body(task);
    }

}