package mx.roomie.resource;

import java.util.List;
import java.util.Optional;

import mx.roomie.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import mx.roomie.model.HouseHold;
import mx.roomie.request.HouseHoldRequest;
import mx.roomie.service.HouseHoldService;

@RestController
public class HouseHoldResource {

    @Autowired
    private HouseHoldService houseHoldService;

    @PostMapping("/household")
    public ResponseEntity<HouseHold> saveHouseHold(@RequestBody HouseHoldRequest request) {
        Application.logger.debug("Creating household");

        HouseHold household = houseHoldService.saveHouseHold(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(household);
    }

    @GetMapping("/households")
    public ResponseEntity<List<HouseHold>> getHouseHolds() {
        Application.logger.debug("Getting all households");

        List<HouseHold> households = houseHoldService.getHouseHolds();
        ResponseEntity<List<HouseHold>> response = ResponseEntity.ok().body(households);
        return response;
    }

    @GetMapping("/household/{id}")
    public ResponseEntity<HouseHold> getHouseHold(@PathVariable Integer id) {
        Application.logger.debug("Getting household with id: " + id);

        Optional<HouseHold> houseHoldOptional = houseHoldService.getHouseHoldById(id);

        if (!houseHoldOptional.isPresent()) {
            Application.logger.warn("The household doesn't exist");
            return ResponseEntity.notFound().build();
        }

        HouseHold houseHold = houseHoldOptional.get();
        return ResponseEntity.ok().body(houseHold);

    }

    @DeleteMapping("/household/{id}")
    public ResponseEntity<HouseHold> deleteHouseHold(@PathVariable Integer id) {
        Application.logger.debug("Deleting household with id: " + id);

        houseHoldService.deleteHouseHold(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping("/household/{id}")
    public ResponseEntity<HouseHold> updateHouseHold(@PathVariable Integer id,
            @RequestBody HouseHoldRequest request) {
        Application.logger.debug("Updating household with id: " + id);
        Optional<HouseHold> houseHoldOptional = houseHoldService.getHouseHoldById(id);

        if (!houseHoldOptional.isPresent()) {
            Application.logger.warn("The household doesn't exist");
            return ResponseEntity.notFound().build();
        }

        HouseHold household = houseHoldService.saveHouseHold(request, id, houseHoldOptional);

        return ResponseEntity.ok().body(household);
    }

}