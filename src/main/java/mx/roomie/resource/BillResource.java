package mx.roomie.resource;

import java.util.List;
import java.util.Optional;

import mx.roomie.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import mx.roomie.model.Bill;
import mx.roomie.request.BillRequest;
import mx.roomie.service.BillService;

@RestController
public class BillResource{
    @Autowired
    private BillService billService;

    @PostMapping("/bill")
    public ResponseEntity<Bill> saveBill(@RequestBody BillRequest request) {
        Application.logger.debug("Creating bill for user: " + request.getUserId());
        Bill bill = billService.saveBill(request);
        
        return ResponseEntity.status(HttpStatus.CREATED).body(bill);
    }
    
    @GetMapping("/bills")
    public ResponseEntity<List<Bill>> getBills() {
        Application.logger.debug("Getting all bills");
        List<Bill> bills = billService.getBills();
        
        ResponseEntity<List<Bill>> response = ResponseEntity.ok().body(bills);
        return response;
    }
        
    @GetMapping("/bill/{id}")
    public ResponseEntity<Bill> getBill(@PathVariable Integer id) {
        Application.logger.debug("Retrieving bill with id: " + id);

        Optional<Bill> billOptional = billService.getBillById(id);

        if (!billOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Bill bill = billOptional.get();

        return ResponseEntity.ok().body(bill);
    }

    @DeleteMapping("/bill/{id}")
    public ResponseEntity<Bill> deleteBill(@PathVariable Integer id) {
        Application.logger.debug("Deleting bill with id: " + id);

        billService.deleteBill(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping("/bill/{id}")
    public ResponseEntity<Bill> updateBill(@PathVariable Integer id, @RequestBody BillRequest request) {
        Application.logger.debug("Updating bill with id: " + id);

        Optional<Bill> billOptional = billService.getBillById(id);

        if (!billOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Bill bill = billService.saveBill(request, id, billOptional);

        return ResponseEntity.ok().body(bill);
    }
}