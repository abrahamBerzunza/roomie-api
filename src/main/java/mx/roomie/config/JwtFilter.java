package mx.roomie.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import mx.roomie.Application;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import io.jsonwebtoken.Jwts;
import mx.roomie.model.User;
import mx.roomie.service.UserService;

public class JwtFilter extends GenericFilterBean {

  private final UserService userService;

  public JwtFilter(UserService userService) {
    this.userService = userService;
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String token = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);

        if (token != null) {
          String email = Jwts.parser()
            .setSigningKey(SecurityConstants.SECRET_KEY)
            .parseClaimsJws(token.replace("Bearer", " "))
            .getBody()
            .getSubject();

          if (email == null) {
              Application.logger.error("Email at the request is null");
              throw new IOException("The mail doesn't have content");
          }

          User user = userService.getUserByEmail(email);

          if (user != null ) {
            Authentication auth = new UsernamePasswordAuthenticationToken(user, null, null);
            SecurityContextHolder.getContext().setAuthentication(auth);
          }
        }

        chain.doFilter(request, response);
  }
  
}