package mx.roomie.repository;

import org.springframework.data.repository.CrudRepository;

import mx.roomie.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {
  User findByEmail(String email);
}