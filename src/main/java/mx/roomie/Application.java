package mx.roomie;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
  public final static Logger logger = Logger.getLogger(Application.class);

  public static void main(String[] args) {
    logger.debug("Starting application");
    SpringApplication.run(Application.class, args);
  }
}