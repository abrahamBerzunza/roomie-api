package mx.roomie.request;

public class TaskRequest {
    private String title;
    private String description;
    // private Integer userId;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    // public Integer getUserId() {
    //     return userId;
    // }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    // public void setUserId(Integer userId) {
    //     this.userId = userId;
    // }
}