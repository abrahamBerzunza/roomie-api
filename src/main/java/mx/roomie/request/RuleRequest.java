package mx.roomie.request;

public class RuleRequest {
    private String title;
    private String description;
    // private Integer householdId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    // public Integer getHouseholdId() {
    //     return householdId;
    // }

    // public void setHouseholdId(Integer householdId) {
    //     this.householdId = householdId;
    // }
}
