package mx.roomie.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class HouseHold {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String street;
    private String colony;
    private String country;
    private String city;
    private String address_line1;
    private String address_line2;
    
    @OneToMany(mappedBy = "household", cascade = CascadeType.ALL)
    private List<User> users;

    @OneToMany(mappedBy = "household", cascade = CascadeType.ALL)
    private List<Rule> rules;

    // @OneToMany(mappedBy = "household", cascade = CascadeType.ALL)
    // private List<Images> images;

    public Integer getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public String getCountry() {
        return country;
    }

    public String getColony() {
        return colony;
    }

    public String getCity() {
        return city;
    }

    public String getAddress_line1() {
        return address_line1;
    }

    public String getAddress_line2() {
        return address_line2;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setColony(String colony) {
        this.colony = colony;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setAddress_line1(String address_line1) {
        this.address_line1 = address_line1;
    }

    public void setAddress_line2(String address_line2) {
        this.address_line2 = address_line2;
    }

    /*
     * @OneToMany(mappedBy = "user_id", cascade = CascadeType.ALL) public List<User>
     * getUsers() { return users; }
     * 
     * public void setUsers(List<User> users) { this.users = users; }
     * 
     * 
     * @OneToMany(mappedBy = "image_id", cascade = CascadeType.ALL) public
     * List<Image> getImages() { return images; }
     * 
     * public void setImages(List<Image> images) { this.images = images; }
     * 
     * @OneToMany(mappedBy = "rule_id", cascade = CascadeType.ALL) public List<Rule>
     * getRules() { return rules; }
     * 
     * public void setRules(List<Rule> rules) { this.rules = rules; }
     */

}