package mx.roomie.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import mx.roomie.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import mx.roomie.model.Bill;
import mx.roomie.repository.BillRepository;
import mx.roomie.request.BillRequest;

@Service
public class BillService {
    @Autowired
    private BillRepository billRepository;

    public List<Bill> getBills() {
        List<Bill> bills = new LinkedList<>();
        billRepository.findAll().iterator().forEachRemaining(bills::add);

        Application.logger.info("Retrieving " + bills.size() + " bills");
        return bills;
    }

    public Optional<Bill> getBillById(Integer id) {
        return billRepository.findById(id);
    }

    public Bill saveBill(BillRequest request, @Nullable Integer id, @Nullable Optional<Bill> billOptional) {
        Bill bill = new Bill();

        if(id != null){
            bill.setId(id);
        }
        bill.setTitle(request.getTitle() != null
            ? request.getTitle()
            : billOptional.get().getTitle());
        bill.setDescription(request.getDescription() != null
            ? request.getDescription()
            : billOptional.get().getDescription());
        bill.setAmount(request.getAmount() != null
            ? request.getAmount()
            : billOptional.get().getAmount());
        // bill.setUserId(request.getUserId() != null
        //     ? request.getUserId()
        //     : billOptional.get().getUserId());

        billRepository.save(bill);

        Application.logger.info("Updated bill with id: " + bill.getId());
        return bill;
    }

    public Bill saveBill(BillRequest request) {
        Bill bill = new Bill();
        
        bill.setTitle(request.getTitle() != null
            ? request.getTitle() : "");
        bill.setDescription(request.getDescription() != null
            ? request.getDescription() : "");
        bill.setAmount(request.getAmount() != null
            ? request.getAmount() : 0);
        // bill.setUserId(request.getUserId() != null
        //     ? request.getUserId() : 0);

        billRepository.save(bill);

        Application.logger.info("Created bill with id: " + bill.getId());
        return bill;
    }

    public void deleteBill(Integer id) {
        billRepository.deleteById(id);
        Application.logger.info("Deleted bill with id: " + id);
    }
}