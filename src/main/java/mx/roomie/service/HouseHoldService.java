package mx.roomie.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import mx.roomie.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import mx.roomie.model.HouseHold;
import mx.roomie.repository.HouseHoldRepository;
import mx.roomie.request.HouseHoldRequest;

@Service
public class HouseHoldService {
    @Autowired
    private HouseHoldRepository houseHoldRepository;

    public List<HouseHold> getHouseHolds() {
        List<HouseHold> houseHolds = new LinkedList<>();
        houseHoldRepository.findAll().iterator().forEachRemaining(houseHolds::add);

        Application.logger.info("Retrieving " + houseHolds.size() + " households");
        return houseHolds;
    }

    public Optional<HouseHold> getHouseHoldById(Integer id) {
        return houseHoldRepository.findById(id);
    }

    public HouseHold saveHouseHold(HouseHoldRequest request) {
        HouseHold household = new HouseHold();

        household.setAddress_line1(request.getAddress_line1());
        household.setAddress_line2(request.getAddress_line2());
        household.setCity(request.getCity());
        household.setColony(request.getColony());
        household.setCountry(request.getCountry());
        household.setStreet(request.getStreet());

        houseHoldRepository.save(household);

        Application.logger.info("Created household: " + household.getId());
        return household;
    }

    public HouseHold saveHouseHold(HouseHoldRequest request, @Nullable Integer id, @Nullable Optional<HouseHold> houseHoldOptional) {
        HouseHold household = new HouseHold();
        household.setId(id);
        household.setAddress_line1(request.getAddress_line1() != null 
            ? request.getAddress_line1()
            : houseHoldOptional.get().getAddress_line1());
        household.setAddress_line2(request.getAddress_line2() != null 
            ? request.getAddress_line2()
            : houseHoldOptional.get().getAddress_line2());
        household.setCity(request.getCity() != null 
            ? request.getCity() 
            : houseHoldOptional.get().getCity());
        household.setColony(request.getColony() != null
            ? request.getColony() 
            : houseHoldOptional.get().getColony());
        household.setCountry(request.getCountry() != null 
            ? request.getCountry()
            : houseHoldOptional.get().getCountry());
        household.setStreet(request.getStreet() != null 
            ? request.getStreet()
            : houseHoldOptional.get().getStreet());

        houseHoldRepository.save(household);

        Application.logger.info("Updated household: " + household.getId());
        return household;
    }

    public void deleteHouseHold(Integer id) {
        houseHoldRepository.deleteById(id);
        Application.logger.info("Deleted household: " + id);
    }
}