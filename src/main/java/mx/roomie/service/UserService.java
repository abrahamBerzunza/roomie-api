package mx.roomie.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import mx.roomie.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Jwts;
import mx.roomie.config.SecurityConstants;
import mx.roomie.model.User;
import mx.roomie.repository.UserRepository;
import mx.roomie.request.UserRequest;

@Service
public class UserService {
  @Autowired
  private UserRepository userRepository;

  public List<User> getUsers() {
    List<User> users = new LinkedList<>();
    userRepository.findAll().iterator().forEachRemaining(users::add);

    Application.logger.info("Retrieving " + users.size() + " users");
    return users;
  }

  public Optional<User> getUserById(Integer id) {
    return userRepository.findById(id);
  }

  public User getUserByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  public User saveUser(UserRequest request) {
    User user = new User();
    user.setUsername(request.getUsername());
    user.setProfilePicture(request.getProfilePicture());
    user.setFirstName(request.getFirstName());
    user.setLastName(request.getLastName());
    user.setEmail(request.getEmail());
    user.setPassword(new BCryptPasswordEncoder().encode(request.getPassword()));

    userRepository.save(user);

    Application.logger.info("Created user: " + user.getEmail());
    return user;
  }

  public User saveUser(UserRequest request, @Nullable Integer id, @Nullable Optional<User> userOptional) {
    User user = new User();
    user.setId(id);
    user.setUsername(request.getUsername() != null 
      ? request.getUsername()
      : userOptional.get().getUsername());
    user.setFirstName(request.getFirstName() != null
      ? request.getFirstName()
      : userOptional.get().getFirstName());
    user.setLastName(request.getLastName() != null
      ? request.getLastName() 
      : userOptional.get().getLastName());
    user.setEmail(request.getEmail() != null
      ? request.getEmail()
      : userOptional.get().getEmail());
    user.setProfilePicture(request.getProfilePicture() != null 
      ? request.getProfilePicture()
      : userOptional.get().getProfilePicture());
    user.setPassword(request.getPassword() != null 
      ? new BCryptPasswordEncoder().encode(request.getPassword())
      : userOptional.get().getPassword());

    userRepository.save(user);

    Application.logger.info("User updated: " + user.getId());

    return user;
  }

  public void deleteUser(Integer id) {
    userRepository.deleteById(id);

    Application.logger.info("User deleted: " + id);
  }

  public String generateToken(String email) {
    String token = Jwts.builder()
      .setSubject(email)
      .signWith(SecurityConstants.SECRET_KEY)
      .compact();

    Application.logger.info("Token generated for user: " + email);

    return token;
  }
}