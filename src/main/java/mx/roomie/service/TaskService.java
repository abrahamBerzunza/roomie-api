package mx.roomie.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import mx.roomie.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import mx.roomie.model.Task;
import mx.roomie.repository.TaskRepository;
import mx.roomie.request.TaskRequest;

@Service
public class TaskService {
    @Autowired
    private TaskRepository taskRepository;

    public List<Task> getTasks() {
        List<Task> tasks = new LinkedList<>();
        taskRepository.findAll().iterator().forEachRemaining(tasks::add);

        Application.logger.info("Retrieving " + tasks.size() + " tasks");

        return tasks;
    }

    public Optional<Task> getTaskById(Integer id) {
        return taskRepository.findById(id);
    }

    public Task saveTask(TaskRequest request) {
        Task task = new Task();
        task.setTitle(request.getTitle());
        task.setDescription(request.getDescription());
        // task.setUserId(request.getUserId());
        taskRepository.save(task);

        Application.logger.info("Creating task: " + task.getId());
        return task;
    }

    public Task saveTask(TaskRequest request, @Nullable Integer id, @Nullable  Optional<Task> taskOptional) {
        Task task = new Task();
        task.setId(id);
        task.setTitle(request.getTitle() != null
            ? request.getTitle() 
            : taskOptional.get().getTitle());
        task.setDescription(request.getDescription() != null
            ? request.getDescription()
            : taskOptional.get().getDescription());
        // task.setUserId(request.getUserId() != null
        //     ? request.getUserId()
        //     : taskOptional.get().getUserId());

        Application.logger.info("Task updated: " + id);

        return task;
    }

    public void deleteTask(Integer id) {
        taskRepository.deleteById(id);

        Application.logger.info("Task deleted: " + id);
    }
}